import { Injectable } from '@angular/core';
import { StorageUtil } from 'src/utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../models/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  
  private _trainer?: Trainer;

  // Getters/ setters
  public get trainer(): Trainer | undefined
  {
    return this._trainer;
  }

  public set trainer(trainer: Trainer | undefined)
  {
    // Save trainer to local storage when the trainer is set
    StorageUtil.saveToStorage<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  // Get trainer from local storage
  constructor() 
  { 
    this._trainer = StorageUtil.readFromStorage<Trainer>(StorageKeys.Trainer);
  }

  // Return if true if the trainer has the pokemonName in the list of pokemon else return false
  public isCollected(pokemonName: string): boolean
  {
    if(this._trainer)
    {
      return Boolean(this.trainer?.pokemon.find((pokemon: string) => pokemon === pokemonName));
    }
    return false;
  }

  // Add a pokemon name to the current list of pokemon from the trainer
  public addToCollection(pokemonName: string): void
  {
    if(this._trainer)
    {
      this._trainer.pokemon.push(pokemonName);
    }
  }

  // Remove a pokemon name from the current list of pokemon from the trainer
  public rmvCollected(pokemonName: string): void
  {
    if(this._trainer)
    {
      let newTrainer = [];

      for(let pokemon of this._trainer.pokemon)
        if(pokemon !== pokemonName) {
          newTrainer.push(pokemon);
        }
      
      this._trainer.pokemon = newTrainer;
    }
  }
}
