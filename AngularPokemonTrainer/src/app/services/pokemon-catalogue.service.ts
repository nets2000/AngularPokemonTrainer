import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs';

import { StorageUtil } from 'src/utils/storage.util';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Pokemon, PokemonAPIResult, PokemonImg, PokemonAPI } from '../models/pokemon.model';

const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _results: PokemonAPIResult[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  // Dependency Injection
  constructor(private readonly http: HttpClient) 
  { 
  }

  // Getters and setters
  get pokemons(): Pokemon[]
  {
    return this._pokemons;
  }

  get pokemonEntries(): PokemonAPIResult[]
  {
    return this._results;
  }

  get error(): string
  {
    return this._error;
  }
  
  get loading(): boolean
  {
    return this._loading;
  }

  set pokemons(pokemons: Pokemon[])
  {
    // Save pokemons to local storage when setting them
    StorageUtil.saveToStorage<Pokemon[]>(StorageKeys.Pokemon, pokemons);
    this._pokemons = pokemons;
  }
  
  // Get all pokemon from the poke api or local storage
  public findPokemon(): void
  {
    // Check if the pokemon exist in local storage 
    if(localStorage.getItem("pokemon-info") === null)
    {
      this._loading = true;
      this.http.get<PokemonAPI>(`${apiPokemon}?limit=2000`)
        .pipe(
          finalize(() => 
          {
            this._loading = false;
          })
        )
        .subscribe(
          {
            next: (pokemonAPI: PokemonAPI) => 
            {
              // get all images of the pokemons
              for(let result of pokemonAPI.results)
              {
                this.getPokemonImg(result);
              }

              // Make sure all the pokemons are correctly stored before setting them
              setTimeout(() => {
                this.pokemons = this._pokemons;
              }, 5000);
            },
            error: (error: HttpErrorResponse) =>
            {
              this._error = error.message;
            }
          });
      }
      else
      {
        // Read pokemon from local storage
        this._pokemons = StorageUtil.readFromStorage("pokemon-info")!;
      }
  }

  // Get the image from a pokemon based on PokemonAPIResult
  public getPokemonImg(result: PokemonAPIResult): void
  {
    // Get image url/ name
    let imgUrl = result.url;
    let pokeName = result.name;

    this.http.get<PokemonImg>(imgUrl)
        .subscribe(
          {
            next: (pokemonImg: PokemonImg) =>
            {
              // Set pokemon name/ image url
              let newPoke: Pokemon = {
                name: pokeName,
                image: pokemonImg.sprites.front_default
              }

              // Fix the url of the images if the url was incorrect (urls could contain https twice)
              if (newPoke.image !== null) {
                const imageURLArray = newPoke.image.split("https");
                if (imageURLArray.length > 2) {
                  if (imageURLArray[2] === null) {return;}
                  const newLink = "https" + imageURLArray[2];
                  newPoke.image = newLink;
                }
              } else {
                // Set image to missing no if pokemon image did not exist
                newPoke.image = "https://static.wikia.nocookie.net/pokemontowerdefense/images/c/ce/Missingno_image.png";
              }

              this._pokemons.push(newPoke);
            },
            error: (error: HttpErrorResponse) =>
            {
              this._error = error.message;
            }
          }
        )
  }

  // Find pokemon by name
  public pokemonByName(pokemonName: string): Pokemon | undefined 
  {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.name === pokemonName);
  }

}
