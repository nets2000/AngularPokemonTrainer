import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

import { Trainer } from '../models/trainer.model';
import { Pokemon } from '../models/pokemon.model';
import { finalize, Observable, tap } from 'rxjs';
import { StorageUtil } from 'src/utils/storage.util';
import { StorageKeys } from '../enums/storage-keys.enum';

const { apiKey, apiTrainers } = environment;
/*
Service that handles the collection status of a pokemon. It communicates with both the
Trainer and Pokemon Catalogue service to set the collection status of the correct Pokemon
for the correct Trainer
*/
@Injectable({
  providedIn: 'root'
})
export class CollectService {
  private _canRemovePokemon: boolean = false;

  private _loading: boolean = false;

  // Getters/ setters
  get loading(): boolean
  {
    return this._loading;
  };

  get canRemovePokemon(): boolean
  {
    return this._canRemovePokemon;
  };

  set canRemovePokemon(canRemovePokemon: boolean)
  {
    this._canRemovePokemon = canRemovePokemon;
  };

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
    private http: HttpClient
  ) { }

  // Get the Pokemon based on the name and patch this request with the corresponding Trainer
  public addToCollected(pokemonName: string): Observable<Trainer>
  {
    if(!this.trainerService.trainer)
      throw new Error("addToCollected: User does not exist")

    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonCatalogueService.pokemonByName(pokemonName);

    if(!pokemon) //In case a pokemon is added that does not exist within the API
      throw new Error(`addToCollected: No pokemon with name: ${pokemonName}`);

    if(this.trainerService.isCollected(pokemonName))
      this.trainerService.rmvCollected(pokemonName);
    else
      this.trainerService.addToCollection(pokemonName);

    const headers = new HttpHeaders(
    {
      'content-type': 'application/json',
      'x-api-key': apiKey
    });

    this._loading = true; //We set this variable just in case it takes too long for the patching to process

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, 
    {
      pokemon: [...trainer.pokemon]
    },
    {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => 
      {
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(() =>
      {
        this._loading = false;
      })
    );
  }

  // Return all the pokemon that the current user has currently collected
  public ReturnCollectedPokemons(): Pokemon[] 
  {
    const collectedPokemon: Pokemon[] = [];
    const allPokemon: Pokemon[] = StorageUtil.readFromStorage(StorageKeys.Pokemon)!;

    for (let i = 0; i < this.trainerService.trainer!.pokemon.length; i++) {
      const newPoke: Pokemon = allPokemon.find(pokemon => this.trainerService.trainer!.pokemon[i] === pokemon.name)!;
      collectedPokemon.push(newPoke);
    }

    return collectedPokemon
  }
}
