import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap} from 'rxjs';

import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiTrainers, apiKey }= environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency Injection
  constructor(private readonly http: HttpClient) { }

  // Login
  public login(username: string): Observable<Trainer>
  {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: Trainer | undefined) =>
        {
          if (user === undefined) // user does not exist
          {  
            return this.createUser(username)
          }
          return of(user);
        })
      );
  }

  // Check if trainer exists - trainer name is specified as username in the API
  // Hence we take argument username of type string
  private checkUsername(username: string): Observable<Trainer | undefined>
  {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(
        // RxJS Operators
        map((response: Trainer[]) => 
        {
          return response.pop();
        })
      )
  }

  // IF NOT user - Create User
  private createUser(username: string): Observable<Trainer>
  {
    // define trainer (=user)
    const user = 
    {
      username,
      pokemon: []
    };

    // headers -> API Key
    const headers = new HttpHeaders(
    {
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
    // POST to server
    return this.http.post<Trainer>(apiTrainers, user, {
      headers
    })
  }
}
