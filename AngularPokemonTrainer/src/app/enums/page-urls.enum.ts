// All the urls for the different screens
export enum PageUrls
{
    LoginPage = "login",
    TrainerPage = "trainer",
    PokemonPage = "pokemons"
}