// enum with labeled keys that are used for the locally stored data
export enum StorageKeys
{
    Trainer = "trainer-name",
    Pokemon = "pokemon-info"
}