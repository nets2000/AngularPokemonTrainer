import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PageUrls } from '../enums/page-urls.enum';
import { TrainerService } from '../services/trainer.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private readonly router: Router,
    private readonly trainerService: TrainerService
  ) {}
  
  // Sends user to login page if user does not exist in local storage else the user can stay on the current page
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.trainerService.trainer) {
      return true;
    } else {
      this.router.navigateByUrl(`/${PageUrls.LoginPage}`)
      return false;
    }
  } 
}
