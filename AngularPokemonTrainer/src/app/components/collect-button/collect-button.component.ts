import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CollectService } from 'src/app/services/collect.services';
import { TrainerService } from 'src/app/services/trainer.service';
/*
Component that handles the collection button within a PokemonCatalogueItem component.
We need a pokemonName input to know which button on which pokemon has been clicked
*/
@Component({
  selector: 'app-collect-button',
  templateUrl: './collect-button.component.html',
  styleUrls: ['./collect-button.component.css']
})
export class CollectButtonComponent implements OnInit {

  public isCollected: boolean = false;
  @Input() pokemonName: string = "";

  constructor(
    private trainerService: TrainerService,
    private readonly collectService: CollectService
  ) { }

  // checks whether the collect service is running or not
  get loading(): boolean
  {
    return this.collectService.loading;
  }

  // Checks what images to render on the button and if the user can remove a pokemon
  get canRemovePokemon(): boolean
  {
    return this.collectService.canRemovePokemon;
  }

  ngOnInit(): void 
  {
    // Called once inputs are resolved, so you can check whether a pokemon
    // has been collected or not.
    this.isCollected = this.trainerService.isCollected(this.pokemonName);
  }

  // handles the action once the collection button is clicked
  onCollectClick(): void
  {
    // Return if the pokemon is already collected and the user is on the pokemon catalogue page
    if (this.isCollected && !this.collectService.canRemovePokemon) { return; }

    this.collectService.addToCollected(this.pokemonName)
      .subscribe(
        {
          next: () =>
          {
            this.isCollected = this.trainerService.isCollected(this.pokemonName);
          },
          error: (error: HttpErrorResponse) =>
          {
            console.log("ERROR", error.message);
          }          
        }
      )
  }

}
