import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectService } from 'src/app/services/collect.services';
import { TrainerService } from 'src/app/services/trainer.service';
/*
This class specifies the pokemon catalog on the pokemon catalog page and serves as
a container of catalogue items.
*/
@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit{
  // Takes input of a Pokemon array which is required to view the items within the catalogue
  @Input() pokemons: Pokemon[] = [];
  
  // Handles when the go to trainer profile button is clicked and emits an event to its container class (PokemonCataloguePage)
  @Output() goToTrainerProfileEvent: EventEmitter<void> = new EventEmitter();

  // Getter
  get trainer(): Trainer | undefined
  {
    return this.trainerService.trainer;
  }

  constructor(
    private readonly trainerService: TrainerService,
    private readonly collectService: CollectService
  ) { }

  // Set specific data for rendering/ function of the collect pokemon collect button
  ngOnInit(): void {
    this.collectService.canRemovePokemon = false;
  }

  // Emit go to trainer page call
  goToTrainerProfile(): void{
    this.goToTrainerProfileEvent.emit();
  }
}
