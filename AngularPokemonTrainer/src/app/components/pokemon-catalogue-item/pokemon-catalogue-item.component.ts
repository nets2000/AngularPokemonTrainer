import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
/*
This component represents an item in the pokemon catalog, which contains the name and image of each pokemon
in the catalogue. Since the pokemon naes are stored in lowercase, the function pokemonName() replaces the
first character with its uppercase variant.
*/
@Component({
  selector: 'app-pokemon-catalogue-item',
  templateUrl: './pokemon-catalogue-item.component.html',
  styleUrls: ['./pokemon-catalogue-item.component.css']
})
export class PokemonCatalogueItemComponent
{
// We need an input of type Pokemon to know what pokemon name and image to display
@Input() pokemon?: Pokemon;

  constructor() { }

  // returns the pokemon name with the 1st letter as uppercase (for better html makeup)
  public pokemonName(pokemonName: string): string
  {
    return pokemonName.charAt(0).toUpperCase() + pokemonName.substring(1);
  }

}
