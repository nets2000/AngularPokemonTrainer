import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectService } from 'src/app/services/collect.services';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: ['./trainer-profile.component.css']
})
export class TrainerProfileComponent implements OnInit {
  @Output() logout: EventEmitter<void> = new EventEmitter();
  @Output() goToPokemonCatalogueEvent: EventEmitter<void> = new EventEmitter();
  
  constructor(
    private readonly trainerService: TrainerService,
    private readonly collectService: CollectService
  ) { }

  // Getters
  get trainer(): Trainer | undefined
  {
    return this.trainerService.trainer;
  }

  get pokemons(): Pokemon[] | undefined
  {
    return this.collectService.ReturnCollectedPokemons();
  }

  // Set specific data for rendering/ function of the collect pokemon collect button
  ngOnInit(): void {
    this.collectService.canRemovePokemon = true;
  }

  // Emit logout call
  public logOut() {
    this.logout.emit();
  }

  // Emit go to catalogue call
  public goToPokemonCatalogue() {
    this.goToPokemonCatalogueEvent.emit();
  }
}
