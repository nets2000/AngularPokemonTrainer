import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { TrainerService } from 'src/app/services/trainer.service';
import { LoginService } from 'src/app/services/login.service';
import { Trainer } from 'src/app/models/trainer.model';

const minimumUsernameLength = 3;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  public loggingInMessage: string = "Logging in...";
  public loggingIn: boolean = false;
  public pressedButton: boolean = false;

  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService
  ) { }

  // Starts the login of the user
  public loginSubmit(loginForm: NgForm): void {
    // Allows the error messages to pop-up if the user didn't fill in a correct username
    this.pressedButton = true;
    
    // Get value and remove white spaces at start and end. After this the user name still needs the minimum length
    let { username } = loginForm.value;
    username = username.trim();
    if (username.length < minimumUsernameLength || this.loggingIn) {
      return;
    }

    // Log in to the program
    this.loggingIn = true;

    this.loginService.login(username)
    .subscribe(
    {
      next: (user: Trainer) => {
        this.trainerService.trainer = user;
        this.login.emit()
      },
      error: () => {
        console.error("Login failed");
      }
    })
  }
}
