// Data that needs to be saved for a pokemon
export interface Pokemon
{
    name: string;
    image: string;
}

// Data as gotten from the api needs to be gotten via the PokemonImg interface
export interface PokemonImg
{
    sprites: { 
        front_default: string; 
    } 
}

// Data as gotten from the api gets stored with this interface
export interface PokemonAPIResult
{
    name: string;
    url: string;
}

// Data as gotten from the pokemon api
export interface PokemonAPI
{
    count: number,
    next: string,
    previous: null,
    results: {name: string, url: string}[]
}