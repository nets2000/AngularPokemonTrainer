// All the data that needs to be saved for a user of the program
export interface Trainer
{
    id: number;
    username: string;
    pokemon: string[];
}