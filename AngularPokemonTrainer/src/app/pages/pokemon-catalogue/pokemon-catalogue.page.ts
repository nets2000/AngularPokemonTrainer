import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageUrls } from 'src/app/enums/page-urls.enum';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
/*
Class that serves as the Pokemon catalogue page. It contains the pokemon catalogue and its items and communicates
with the pokemon catalogue service to load in the items.
*/
@Component({
  selector: 'app-pokemon-catalogue-page',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService,
    private readonly router: Router
  ) { }

  // Getters/ setters
  get pokemons(): Pokemon[]
  {
    return this.pokemonCatalogueService.pokemons;
  }

  get error(): string
  {
    return this.pokemonCatalogueService.error;
  }

  get loading(): boolean
  {
    return this.pokemonCatalogueService.loading;
  }

  // Get all pokemon from the pokeAPI or local storage
  ngOnInit(): void 
  {
    this.pokemonCatalogueService.findPokemon();
  }

  // routes to the profile page once the 'Go to Trainer page' button is clicked
  handleGoToTrainerProfile(): void {
    this.router.navigateByUrl(`/${PageUrls.TrainerPage}`);
  }
}
