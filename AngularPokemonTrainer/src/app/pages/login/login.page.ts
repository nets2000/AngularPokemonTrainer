import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageUrls } from 'src/app/enums/page-urls.enum';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { StorageUtil } from 'src/utils/storage.util';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {
  constructor(private readonly router: Router) { }

  // Send user to pokemon catalogue if a user exists in the local storage
  ngOnInit(): void {
    if (StorageUtil.checkIfUserExists(StorageKeys.Trainer)) {
      this.handleLogin();
    }
  }

  // Send user to pokemon catalogue 
  handleLogin(): void {
    this.router.navigateByUrl(`/${PageUrls.PokemonPage}`);
  }
}
