import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageUrls } from 'src/app/enums/page-urls.enum';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { StorageUtil } from 'src/utils/storage.util';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }

  // Clears the user from the local storage
  handleLogout(): void {
    StorageUtil.clearLocalStorage(StorageKeys.Trainer, this.router);
  }

  // Sends user to pokemon catalogue page
  handleGoToPokemonCatalogue(): void {
    this.router.navigateByUrl(`/${PageUrls.PokemonPage}`);
  }
}
