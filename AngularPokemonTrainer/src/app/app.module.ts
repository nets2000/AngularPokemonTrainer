import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { PokemonCatalogueComponent } from './components/pokemon-catalogue/pokemon-catalogue.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { TrainerProfileComponent } from './components/trainer-profile/trainer-profile.component';
import { FormsModule } from '@angular/forms';
import { PokemonCatalogueItemComponent } from './components/pokemon-catalogue-item/pokemon-catalogue-item.component';
import { CollectButtonComponent } from './components/collect-button/collect-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    PokemonCataloguePage,
    PokemonCatalogueComponent,
    LoginFormComponent,
    TrainerProfileComponent,
    PokemonCatalogueItemComponent,
    CollectButtonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
