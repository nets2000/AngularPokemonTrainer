import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PageUrls } from "./enums/page-urls.enum";
import { AuthGuard } from "./guards/auth.guard";
import { LoginPage } from "./pages/login/login.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerPage } from "./pages/trainer/trainer.page";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: PageUrls.LoginPage
    },
    {
        path: PageUrls.LoginPage,
        component: LoginPage
    },
    {
        path: PageUrls.TrainerPage,
        component: TrainerPage,
        canActivate: [ AuthGuard]
    },
    {
        path: PageUrls.PokemonPage,
        component: PokemonCataloguePage,
        canActivate: [ AuthGuard]
    }
]

@NgModule ({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}