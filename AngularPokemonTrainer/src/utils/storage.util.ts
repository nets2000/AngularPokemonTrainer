import { Router } from "@angular/router";
import { PageUrls } from "src/app/enums/page-urls.enum";
/*
Util class that handles writing to and reading from the local storage within the browser
*/
export class StorageUtil
{
    // Save data to local storage with given key
    public static saveToStorage<T>(key: string, value: T): void
    {
        localStorage.setItem(key, JSON.stringify(value));
    }

    // Read data from local storage from given key
    public static readFromStorage<T>(key: string): T | undefined
    {
        const storedValue = localStorage.getItem(key);
        try
        {
            if(storedValue) {
                return JSON.parse(storedValue) as T;
            }

            return undefined;
        }
        catch(err)
        {
            localStorage.removeItem(key);
            return undefined;
        }
    }

    // Check if a user exists
    public static checkIfUserExists(key: string): boolean 
    {
        const item = localStorage.getItem(key);
        return item !== null ? true : false;
    }

    // Remove given key and its data from local storage
    public static clearLocalStorage(key: string, router: Router): void
    {
        localStorage.removeItem(key);
        // Always send user back to login when removed from local storage
        router.navigateByUrl(`/${PageUrls.LoginPage}`);
    }
}