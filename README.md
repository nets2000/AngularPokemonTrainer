# Angular Pokemon Trainer

An app where the player can catch pokemon by clicking on a button to add them to their collection. You can then view this collection and remove pokemon from this collection by clicking on a button. To create an account a trainer name is required and nothing more!

# Installation & start-up

Fork to project to you're own git repository or download the zip file.<br>
On first start-up of the program create in the 'src' folder a new folder called 'environments'. In this folder create two files called: <br>
1. "environment.ts",<br>
2. "environment.prod.ts"<br>

![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/EnvironmentExplanation1.PNG?raw=true)<br>
In the environment.ts file the following code must be written:<br>
![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/EnvironmentExplanation2.PNG?raw=true)<br>
"You're api" must be exchanged with the api you have for the program.<br>
"You're key" must be exchanged with the key you have for you're api for the program.<br>
The apiPokemon variable can stay the same. This is a public database where data for pokemon is stored.<br>
In the environment.prod.ts file the following code must be written:<br>
![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/EnvironmentExplanation3.PNG?raw=true)<br>
"You're api" must be exchanged with the api you have for the program.<br>

From there open the terminal in the root folder (ctr + shift + `) and type:<br>
'cd AngularPokemonTrainer' in the terminal<br>
Then type:<br>
npm i<br>
This will install all dependencies for the project. To start the program type:<br>
ng serve<br>
Always be sure to be in the AngularPokemonTrainer folder before typing ng serve, else the program won't work.

# Usage
The first screen you will see is this:
![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/LoginScreen.PNG?raw=true)<br>
In this screen you can fill in you're username. This is required and has a minimum lenght of 3 and a maximum lenght of 12. This is all that is required for creating a account.<br>
The next screen you will see is the pokemon catch screen:
![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/PokemonCatchScreen.PNG?raw=true)<br>
In this screen you can click on the pokeballs in the boxes of the pokemon to add the pokemon to you're collection. Every pokemon can only be added once. By clicking on the "Go to trainer profile" button you can go to the profile page.
![alt text](/AngularPokemonTrainer/src/assets/ReadMeImages/ProfileScreen.PNG?raw=true)<br>
In this screen you can see all the pokemon you added to you're collection. By clicking on the red cross in the box of the pokemon you can remove the pokemon from you're collection and this opens it up again to be added from the pokemon
catch screen. You can also go back to the pokemon catch screen by clicking the "Catch some pokemon" button and log out by clicking the "log out" button.

# Colleberators

brianhuynen: https://gitlab.com/brianhuynen
